let http = require('http');
let url = require('url');
let port = process.argv[2];

let server = http.createServer((request, response) => {
  if (request.method !== 'GET') { return res.end('Send me a GET\n') }
  let parsedUrl = url.parse(request.url, true);
  let date = new Date(parsedUrl.query.iso);
  let result;
  
  if (parsedUrl.pathname == '/api/parsetime') {    
    result = {
      "hour" : date.getHours(),
      "minute" : date.getMinutes(),
      "second" : date.getSeconds()
    } 
  } else if (parsedUrl.pathname == '/api/unixtime') {
    result = {
      "unixtime" : date.getTime()
    }
  }

  if (result) {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.end(JSON.stringify(result));
  } else {
    rresponsees.writeHead(404);
    response.end();
  }
});
server.listen(port);

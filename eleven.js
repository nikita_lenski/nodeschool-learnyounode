let http = require('http');
let fs = require('fs');
let port = process.argv[2];
let file = process.argv[3];

let server = http.createServer((request, response) => {
  response.writeHead(200, { 'content-type': 'text/plain' });  
  let data = fs.createReadStream(file);
  data.pipe(response);
  response.on('error', console.error)
});
server.listen(port);

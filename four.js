let fs = require('fs');
let res;
let cons = (pr) => console.log(pr);

let syncRide = (callback) => {
  fs.readFile(process.argv[2], function doneReading(err, data) {
    try {      
      res = (data.toString().split('\n')).length - 1;
      // console.log('res', res);
      callback(res)
    } catch(e){
      console.log('Error -> ', e);
    }
  })
}
syncRide(cons);

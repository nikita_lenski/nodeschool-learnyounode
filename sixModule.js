let fs = require('fs');

module.exports = syncRide = (dir, ext, callback) => {
  fs.readdir(dir, (error, data) => {
    // if (error) return console.error('Error -> ', error);
    if (error) return callback(error);

    let res = data.filter((item) => {
      let index = item.indexOf('.');
      let pas = item.slice(index + 1);
      if ( index != -1 && pas == ext) {
        return item;
      }
    });

    // console.log('res', res);

    callback(null, res);

  })
}

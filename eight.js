let http = require('http');
let bl = require('bl');
let url = process.argv[2];

http.get(url, (response) => {
  response.setEncoding('utf8');
  response.pipe(bl((error, data) => {
    if (error) return console.error('Error -> ', error);
    data = data.toString();
    console.log(data.length);
    console.log(data);
  }))
  response.on('error', console.error)
}).on('error', console.error);

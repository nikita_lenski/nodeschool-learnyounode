let http = require('http');
let bl = require('bl');
let url1 = process.argv[2];
let url2 = process.argv[3];
let url3 = process.argv[4];

var results = []
var count = 0

const printResults = () => {
  for (var i = 0; i < 3; i++) {
    console.log(results[i])
  }
}

for(let i = 0; i < 3; i++) {
  http.get(process.argv[2 + i], (response) => {
    response.setEncoding('utf8');
    response.pipe(bl((error, data) => {
      if (error) return console.error('Error -> ', error);
      results[i] = data.toString();
      count++;
      if (count === 3) {
        printResults();
      }
    }))
    response.on('error', console.error)
  }).on('error', console.error);
}


// [url1, url2, url3].map( item => {
//   http.get(item, (response) => {
//         response.setEncoding('utf8');
//         response.pipe(bl((error, data) => {
//           if (error) return console.error('Error -> ', error);
//           data = data.toString()
//           console.log(data);
//         }))
//         response.on('error', console.error)
//       }).on('error', console.error);

// })



// const res1 = Promise.all(async () => {    

//   let res1 = await http.get(url1, (response) => {
//     response.setEncoding('utf8');
//     return response.pipe(bl((error, data) => {
//       if (error) return console.error('Error -> ', error);
//       data = data.toString()
//       console.log('data1', data);  
//       return data;
//     }))
//     response.on('error', console.error)
//   }).on('error', console.error);

//   console.log('res1', res1);   


// });

// let allResp = async () => {

//   let res1 = await http.get(url1, (response) => {
//     response.setEncoding('utf8');
//     return response.pipe(bl((error, data) => {
//       if (error) return console.error('Error -> ', error);
//       data = data.toString()
//       console.log('data1', data);  
//       return data;
//     }))
//     response.on('error', console.error)
//   }).on('error', console.error);

//   let res2 = await http.get(url2, (response) => {
//     response.setEncoding('utf8');
//     response.pipe(bl((error, data) => {
//       if (error) return console.error('Error -> ', error);
//       data = data.toString()
//       console.log('data2', data);  
//       // return data;
//     }))
//     response.on('error', console.error)
//   }).on('error', console.error);

//   let res3 = await http.get(url3, (response) => {
//     response.setEncoding('utf8');
//     response.pipe(bl((error, data) => {
//       if (error) return console.error('Error -> ', error);
//       data = data.toString()
//       console.log('data3', data);  
//       // return data;
//     }))
//     response.on('error', console.error)
//   }).on('error', console.error);

//   // console.log('res1', res1);  
// }

// allResp();
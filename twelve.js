let http = require('http');
let map = require('through2-map');
let port = process.argv[2];

let server = http.createServer((request, response) => {
  if (request.method !== 'POST') { return res.end('Send me a POST\n') }

  let data = request.pipe(map( (chunk) => {
    return chunk.toString().toUpperCase();
  })).pipe(response);

  response.writeHead(200, { 'content-type': 'text/plain' });    
  response.on('error', console.error)
});
server.listen(port);

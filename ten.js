let http = require('http');
let bl = require('bl');
let net = require('net');
let port = process.argv[2];

let zeroFill = (i) => { return (i < 10 ? '0' : '') + i }

let server = net.createServer((socket) => {
  let date = new Date();
  let str = zeroFill(date.getFullYear()) + '-' + 
    zeroFill(date.getMonth() + 1) + '-' + 
    zeroFill(date.getDate()) + ' ' +
    zeroFill(date.getHours()) + ':' +
    zeroFill(date.getMinutes()) + '\n';
  socket.end(str)
});
server.listen(port);

// console.log('server', server);
